FROM debian

RUN apt-get update
RUN apt-get install -y wget

RUN mkdir /src
WORKDIR /src

RUN wget https://github.com/acaudwell/Gource/releases/download/gource-0.49/gource-0.49.tar.gz
RUN tar xzf gource-0.49.tar.gz
RUN mv gource-0.49/* .

# Gource build dependencies
RUN apt-get install -y libsdl2-dev libsdl2-image-dev libpcre3-dev libfreetype6-dev  libglew-dev  libglm-dev libboost-filesystem-dev libpng-dev libtinyxml-dev
RUN apt-get install -y binutils gawk g++ make

# Build
RUN ./configure --with-tinyxml
RUN make
RUN make install

RUN apt-get install -y i965-va-driver
RUN apt-get install -y git


RUN useradd user -u 1000
USER user
